# Magic Number

Instructions of the problem:

- Take a number of **4 digits**. The only restriction is that all the digits can't be the same. So, 2222 or 7777 are not allowed.
- Is important that digits lower than 1000 are allowed. In this case, you have to consider the number with as 0 padding to the left as you need. So numbers like 0003, *0047* or *0659* are also allowed.
- With that number:
	- **Get the greatest** number that you can make with all the 4 digits
	- **Get the lowest** number that you can make with all the 4 digits
	- **Make the subtraction** of the previous numbers: the greatest number - the lowest number

- **With the result, make the same**. Get the greatest and the lowest number and subtract both numbers.
- **Repeat** this process as many times as you need until **the number got after the subtract is the same that the previous one got.** This will be the magical number, and ALL the 4 digits numbers after this process reach this magical number, and always the same. So, there is ONLY ONE magical number if you take 4 digit numbers (and yes, this is possible!)
 

```
Here you have and example. Take the number 8597:
 
- Greatest: 9875 / Lowest: 5789 => Subtract: 4086
- With the 4086: Greatest: 8640 / Lowest: 0468 => Subtract: 8172
- With the 8172: Greatest: 8721 / Lowest: 1278 => Subtract: 7443
- Go on until you get a number WXYZ that if you get the subtract of the greatest and the lowest, you get again the WXYZ number

```

You have to answer these questions:

- Which is the magical number for the numbers with 4 digits?
- How many steps you need as maximum to reach that number? A step is one iteration of the process: get the greatest and the lowest, make the substraction and compare if that number is the same as the original number.
- Suppose that your previous answer is 12 steps. This means that there is a 4 digit number that needs 12 iterations of the process to get the magical number. But sure there are other 4 digit numbers that needs the same steps, and of course, for 11, 10, 9…, steps there are other numbers. So, taking account your previous answer, beginning in 1 step (if is possible to get the magical number in only 1 step) how many 4 digit numbers need only 1 step, how many need 2 steps, 3 steps, etc…, until the maximum of the steps?

-----

## Solution

This is a simple implementation of the algorithm that allows you to answer the questions through an interactive prompt.

**Update**: Now you can check if MagicNumber exists for n given digits. For that, just change the value of the variable DIGITS_LENGTH before compiling and executing the program. The usage is the same as described above. If the program returns StackOverflowError, that means that the recursive function entered an infinite loop and that **MagicNumber** doesn't exists for the given digits length.

Based on test, it seems that for 1..10 digits, **magicNumber only exists for 3 and 4 digits.**

- For the first 2 questions, **use option 1** to check what is the magic number, and the steps needed to reach it for any number between -9999 and 9999. The algorithm will output the magic number and the steps needed for that number, and you can prove that indeed magicNumber has always the same digits! (magicNumber is positive for the positive numbers, and viceversa).
- For the third question, **use option 2**. It will show you how many 4 digit numbers (including negatives) need *0, 1, ... n-until-max* steps to reach magicNumber.
