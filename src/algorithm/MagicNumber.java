package algorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class MagicNumber {

    private static final int DIGITS_LENGTH = 3;

    public static void main(String[] args) throws IOException {

        while (true) {

            System.out.println();
            System.out.println("Please select options: ");
            System.out.println("1. Calculate magicNumber and steps for a single " + DIGITS_LENGTH + " digit number");
            System.out.println("2. How many steps needs all " + DIGITS_LENGTH + " digits numbers?");
            System.out.println("0. Exit");
            System.out.printf("> ");

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String option = reader.readLine();

            try {
                int optionNumber = Integer.parseInt(option);

                switch (optionNumber) {
                case 1:
                    optionMenu1();
                    continue;
                case 2:
                    calculateMagicSteps();
                    continue;
                case 0:
                    System.out.println("Bye!");
                    return;
                default:
                    System.out.println("Error: Not a valid option.");
                    continue;
                }

            } catch (NumberFormatException e) {
                System.out.println("Error: Not a valid option.");
                continue;
            }

        }

    }

    static void optionMenu1() throws IOException {

        System.out.printf("Input number: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String inputNumber = reader.readLine();
        int sign = 1;

        // check for negatives:
        if (inputNumber.startsWith("-")) {
            inputNumber = inputNumber.substring(1);
            sign = -1;
        }

        if (inputNumber.length() <= 0 || inputNumber.length() > DIGITS_LENGTH) {
            System.out.println("Not a valid number: It must have " + DIGITS_LENGTH + " digits");
            return;
        }

        try {
            int number = Integer.parseInt(inputNumber);
            calculateMagicNumber(number, sign);
        } catch (NumberFormatException e) {
            System.out.println("Not parseable integer");
            return;
        }
    }

    static void calculateMagicNumber(int number, int sign) {

        // Before we start we introduce a control to check
        // that a number is valid (1111 not valid, 2222 not valid, etc)
        if (areAllDigitsEquals(number)) {
            System.out.println(number + " is not a valid magicNumber starter, all digits are equal");
            return;
        }

        boolean printMagicNumber = true;
        int steps = calculateMagicStepsRecursive(number, sign, 0, printMagicNumber);
        System.out.println("The steps needed for " + number + " were: " + steps);

    }

    static void calculateMagicSteps() {
        boolean printMagicNumber = false;
        HashMap<Integer, Integer> stepsMap = new HashMap<Integer, Integer>();

        for (int n = (minDigitsRange(DIGITS_LENGTH) + 1); n < maxDigitsRange(DIGITS_LENGTH); n++) {
            int sign = 1;

            if (n < 0) {
                sign = -1;
            }
            int number = n * sign;

            if (areAllDigitsEquals(number)) {
                continue;
            }

            int steps = calculateMagicStepsRecursive(number, sign, 0, printMagicNumber);
            int counter;

            if (stepsMap.containsKey(steps)) {
                counter = stepsMap.get(steps);
                counter++;
            } else {
                counter = 1;
            }

            stepsMap.put(steps, counter);

        }

        // Print Results
        stepsMap.entrySet()
                .forEach(entry -> {
                    System.out.println("There is " + entry.getValue() + " numbers that reach magic in " + entry.getKey()
                            + " steps");
                });

    }

    static boolean areAllDigitsEquals(int number) {

        int[] digits = numberToDigits(number);

        for (int i = 0; i < digits.length - 1; i++) {
            if (digits[i] != digits[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static int calculateMagicStepsRecursive(int number, int sign, int steps, boolean printMagicNumber) {

        int original = number;

        int[] digits = numberToDigits(number);

        Arrays.parallelSort(digits);
        int lowest = getLowest(digits) * sign;
        int greatest = getGreatest(digits) * sign;

        int substract = greatest - lowest;

        if (substract * sign == original) {
            if (printMagicNumber) {
                System.out.println("The magic number is: " + substract);
            }
            return steps;
        }
        // else
        steps++;
        substract *= sign;
        return calculateMagicStepsRecursive(substract, sign, steps, printMagicNumber);

    }

    static int getLowest(int[] digits) {
        int lowest = 0;

        for (int i = 0; i < digits.length; i++) {
            lowest = lowest * 10 + digits[i];
        }
        return lowest;
    }

    static int getGreatest(int[] digits) {
        int greatest = 0;

        for (int i = digits.length - 1; i >= 0; i--) {
            greatest = greatest * 10 + digits[i];
        }
        return greatest;
    }

    static int[] numberToDigits(int number) {

        String formatter = "%0" + DIGITS_LENGTH + "d";
        String numberString = String.format(formatter, number);
        int[] digits = new int[numberString.length()];

        for (int i = 0; i < numberString.length(); i++) {
            digits[i] = Character.getNumericValue(numberString.charAt(i));
        }
        return digits;
    }

    static int maxDigitsRange(int digits) {
        return (int) Math.pow(10, digits);
    }

    static int minDigitsRange(int digits) {
        return (int) Math.pow(10, digits) * -1;
    }

}
